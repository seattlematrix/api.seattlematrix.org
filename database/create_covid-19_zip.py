#!/usr/bin/env python
#
#
import psycopg2
import creds

# assign secrets to variables
dbhost = creds.dblogin['host']
database = creds.dblogin['db']
username = creds.dblogin['username']
password = creds.dblogin['password']
port = creds.dblogin['port']
sslmode = creds.dblogin['sslmode']

conn = psycopg2.connect(
    (f"host={dbhost} dbname={database} user={username} "
     f"password={password} port={port} sslmode={sslmode}")
)

cur = conn.cursor()
cur.execute("""
    CREATE TABLE king_county_covid19_zip(
    id SERIAL PRIMARY KEY,
    date_added DATE NOT NULL DEFAULT CURRENT_DATE,
    date_data DATE,
    zipcode TEXT,
    population INTEGER,
    tests INTEGER,
    test_rate REAL,
    all_test_results REAL,
    all_test_results_rate REAL,
    positives INTEGER,
    positive_rate REAL,
    probable_cases INTEGER,
    probable_case_rate REAL,
    hospitalizations INTEGER,
    hospitalization_rate REAL,
    deaths INTEGER,
    death_rate REAL)
""")
conn.commit()
