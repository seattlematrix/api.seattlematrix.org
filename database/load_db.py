#!/usr/bin/env python
#
#
import os
import re
import sys
from urllib.request import urlopen, HTTPError
import calendar

from bs4 import BeautifulSoup
import numpy as np
import pandas as pd
from sqlalchemy import create_engine
import click
from loguru import logger

# Local Imports
import creds

# assign secrets to variables
dbhost = creds.dblogin['host']
database = creds.dblogin['db']
username = creds.dblogin['username']
password = creds.dblogin['password']
port = creds.dblogin['port']

column_map = {
    'location_name': 'zipcode',
    'people_tested': 'tests',
    'people_tested_rate': 'test_rate',
    'confirmed_cases': 'positives',
    'confirmed_case_rate': 'positive_rate',
}

df_types = {
    'zipcode': 'string',
    'population': 'int32',
    'tests': 'int32',
    'test_rate': 'float32',
    'all_test_results': 'float32',
    'all_test_results_rate': 'float32',
    'positives': 'int32',
    'positive_rate': 'float32',
    'hospitalizations': 'int32',
    'hospitalization_rate': 'float32',
    'deaths': 'int32',
    'death_rate': 'float32',
}

base_url = 'https://www.kingcounty.gov/depts/health/covid-19/data'
daily_summary = f"{base_url}/daily-summary.aspx"
excelnames = [
    "overall-counts-rates-geography",
    "overall_geo",
]

new_cols = [
    'all_test_results',
    'all_test_results_rate',
    'probable_cases',
    'probable_case_rate',
]

sql_get_keys = """
SELECT to_char(date_data, 'YYYYMMDD') as date_data, zipcode
FROM king_county_covid19_zip;
"""

eng = create_engine(
    f'postgresql://{username}:{password}@{dbhost}:{port}/{database}'
)

months_abbr = {v.lower(): k for k, v in enumerate(calendar.month_abbr) if v}
months = {v.lower(): k for k, v in enumerate(calendar.month_name) if v}

res = urlopen(daily_summary)
soup = BeautifulSoup(res, 'html.parser')


def get_data_url():
    """
    Function to parse the excel data url from the King County data
    summary page.
    """
    fn = ''
    searchregex = '|'.join(excelnames)
    for link in soup.findAll('a', attrs={'href': re.compile(searchregex)}):
        fn = link.get('href')
        break

    if fn:
        return f"{base_url}/{fn}"
    else:
        return None


def parse_update_year():
    """
    Function to parse the latest upload year from the King County site.
    """

    for header in soup.findAll('h4'):
        if 'Download COVID-19 data files' in header.text:
            return re.findall(r'\d{4}', header.text)[0]

    raise ValueError(f'Problem parsing the year from site: {daily_summary}')


def parse_date(filename):
    """
    Function to parse out the month and date from the King County daily
    filename.
    """

    extn = filename.split('.')[-1]

    # replace the non-date info from basename
    m_str, d_str = os.path.basename(filename).replace(
        f".{extn}",
        ''
    ).split('-')[-2:]

    try:
        m_int = months_abbr[m_str.lower()[0:3]]
    except KeyError:
        try:
            m_int = months[m_str.lower()]
        except Exception:
            raise ValueError(f"Problem parsing month number from '{m_str}'")

    d_int = int(d_str)

    year = parse_update_year()

    return f"{year}{m_int:02}{d_int:02}"


def drop_db_dupes(df):
    """
    Function to remove duplicate rows (relative to database) from dataframe.
    """

    db_keys = [(r['date_data'], r['zipcode'])
               for i, r in pd.read_sql(sql_get_keys, eng).iterrows()]
    indx = df.apply(
        lambda r: (r['date_data'], r['zipcode']) in db_keys,
        axis=1
    )
    return df[~indx]


@click.command()
@click.option('-f', '--filename', help='Read data from custom path.',
              default=None)
def main(filename):
    """
    CLI command to pull current data from King County website and load into
    database. Intended to be run once a week.
    """

    # Get current data from file or url
    if filename is None:
        filename = get_data_url()

    logger.info(f"loading data from file: {filename}")

    # pull data from King County site
    try:
        df = pd.read_excel(filename, sheet_name='ZIP')
    except HTTPError:
        msg = (f"Error getting data for provided year or "
               f"filename: {filename}")
        raise IOError(msg)

    # Dropping county-wide summary
    df.drop(0, inplace=True)
    # reformat the columns, data types, and nondata values
    df.columns = df.columns.map(str.lower)
    df.rename(columns=column_map, inplace=True)
    # Add a few columns if not present
    for c in new_cols:
        if c not in df.columns:
            df[c] = np.nan

    df.replace(['       .', '.'], value=np.nan, inplace=True)
    df = df.astype(df_types)

    # Add date
    df['date_data'] = parse_date(filename)

    df = drop_db_dupes(df)

    if df.shape[0]:
        # load to database
        df.to_sql(
            'king_county_covid19_zip',
            con=eng,
            if_exists='append',
            index=False
        )
    else:
        sys.stderr.write('No new data to upload.\n')


if __name__ == '__main__':
    main()
