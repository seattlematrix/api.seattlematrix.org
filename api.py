import flask
from flask import request, jsonify, make_response
import psycopg2
import psycopg2.extras
import json
import pandas as pd
from sqlalchemy import create_engine

app = flask.Flask(__name__)
#app.config["DEBUG"] = True
import creds

# Assign secrets to variables, should look into making this 
# a function. Plan to move this to Vault from creds file.
dbhost = creds.dblogin['host']
database = creds.dblogin['db']
username = creds.dblogin['username']
password = creds.dblogin['password']
port = creds.dblogin['port']
sslmode = creds.dblogin['sslmode']

connection_str = (f"host={dbhost} dbname={database} user={username} "
                  f"password={password} port={port} sslmode={sslmode}")

sqlalq_str = f"postgresql://{username}:{password}@{dbhost}:{port}/{database}"



@app.route('/', methods=['GET'])
def home():
    # TODO: Look into putting all this into a template or something.
    return """<h1>Public API offered by the team at Seattle Matrix</h1> <p>This \
        site is a collection of api's from various public sources collected \
        by the team at <a href="https://seattlematrix.org" target="_blank">SeattleMatrix.org</a>.</p> Current Data: \
        </br><a href="https://api.seattlematrix.org/v1/kingcounty/covid-19/zipcode/all" target="_blank">King County, WA Covid-19 By ZipCode - ALL</a>
        </br><a href="https://api.seattlematrix.org/v1/kingcounty/covid-19/zipcode/98104" target="_blank">King County, WA Covid-19 By ZipCode - Filtered on Zipcode 98014 (as example of zipcode filter)</a> </br>
        Current Dates: 2020-08-10, 2020-08-24, 2020-08-31, 2020-09-08, 2020-09-14, 2020-09-30, 2020-10-05, 2020-10-14, 2020-10-19, 2020-12-14, 2020-12-29, 2021-01-19, 2021-02-10, 2021-02-17, 2021-03-03, 2021-03-10, 2021-05-08, 2021-08-30
        """

# A route to return all of the available records for King County COVID-19
@app.route('/v1/kingcounty/covid-19/zipcode/all', methods=['GET'])
def api_zipcode_all():
    """ This route returns all the data in the king_county_covid19_zip
    table. This data is currently manually updated. 
    """

    # SQL statements for the different queries. 

    _sql_all = "SELECT * FROM king_county_covid19_zip;"
    _sql_all_format_date = ("SELECT *, "
        "to_char(date_added, 'YYYYMMDD') as date_added, "
        "to_char(date_data, 'YYYYMMDD') as date_data "
        "FROM king_county_covid19_zip;")

    dataformat = request.args.get('format', default='json', type=str)

    if dataformat == 'json':
        conn = psycopg2.connect(connection_str)
        cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        cur.execute(_sql_all_format_date)
        
        return jsonify(cur.fetchall())

    else:
        df = pd.read_sql(_sql_all, create_engine(sqlalq_str))
        content = df.to_json(orient='table', index=False).encode('utf8')
        response = make_response(content)
        response.headers['Content-Type'] = 'application/json'
        response.headers['Content-length'] = len(content)
        
        return response


# A route to return results based on zipcode
@app.route(
    '/v1/kingcounty/covid-19/zipcode/<int:zipcode>',
    methods=['GET'])
def api_zipcode(zipcode):
    """ This route limits api return results to the Zipcode provided
    """

    # checking for the format request
    dataformat = request.args.get('format', default='json', type=str)

    if dataformat == 'json':
        _sql_zipcode_format_date = """
        SELECT *,
            to_char(date_added, 'YYYYMMDD') as date_added,
            to_char(date_data, 'YYYYMMDD') as date_data
        FROM king_county_covid19_zip WHERE zipcode = '%s';
        """
        # If json return standard json results with date's formatted
        conn = psycopg2.connect(connection_str)
        cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        cur.execute(_sql_zipcode_format_date, (zipcode, ))

        return jsonify(cur.fetchall())

    else:
        _sql_zipcode = """
        SELECT * FROM king_county_covid19_zip WHERE zipcode = '%(zipcode)s';
        """
        df = pd.read_sql(
            _sql_zipcode,
            create_engine(sqlalq_str),
            params={'zipcode': zipcode}
        )
        content = df.to_json(orient='table', index=False).encode('utf8')
        response = make_response(content)
        response.headers['Content-Type'] = 'application/json'
        response.headers['Content-length'] = len(content)

        return response


if __name__ == "__main__":
    app.run(host='0.0.0.0')

# TODO: Create route for filtering on date 
# TODO: Put all above into function for covid-19 data 
# TODO: Look into making a Class for db connection
