# api.seattlematrix.org

API to help make public data easier to access. Idea started with need for King County, WA COVID-19 data in API format while only having an excel file for source data. 

## Development Setup

### Datastore

I am using Postgresql for the datastore due to what I read about concurrancy over mysql, figured that would be good for an API. During development using a container on my machine. 

```bash 
docker run --rm   --name pg-docker -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data  postgres:11
```

You can use this command to connect to the postgresql instance in the container. 

```bash
docker exec -it pg-docker bash
```

From the shell login to the db and setup initial user and database

```bash
psql -U postgres
```

Create your creds.py from the creds_sample.py

```bash
cp creds_sample.py creds.py
```

Edit creds.py with correct info for your db.

First create the database tables via:

```bash
python create_covid-19_zip.py
```

Tables are then populated with with data via `load_db.py`:

```bash
# For help docs
./load_db.py --help
# For single file
./load_db.py -f FILENAME
# For current data on King County website
./load_db.py
```

Note: all database connection info is pulled from `creds.py`.

### Development Env.

Project is using Python 3.8

In the project we use [pipenv](https://pipenv.pypa.io/en/latest/) when developing. 

```bash
pipenv install --dev && pipenv shell
```

[IPython](https://ipython.org/) is included in the dev install. It has some nice features:
  * Nicer looking autocomplete
  * [magic commands](https://ipython.readthedocs.io/en/stable/interactive/magics.html)
  * `?` shorthand for [help docs](https://ipython.readthedocs.io/en/stable/interactive/python-ipython-diff.html#accessing-help)
  * Module [auto-reloading](https://ipython.org/ipython-doc/3/config/extensions/autoreload.html)

For example, start by running `ipython` from the root project directory:

```python
# autoreload extension
%load_ext autoreload
%autoreload 2
from database import load_db
# lookup help on functions
load_db.get_data_url?

# edit the load_db.py file and it will auto reload in this shell without having to re-import

# can also load the app API
import api
api.api_zipcode_all?
```


### Production

When running on Debian server libpq-dev is required, this is postgresl library. 

```bash
sudo apt-get install libpq-dev
```

### Data Available

- King County, Washington COVID-19 Data
    - [King County, WS Covid-19 by ZipCode - ALL ](https://api.seattlematrix.org/v1/kingcounty/covid-19/zipcode/all)
    - [ King County, WA Covid-19 By ZipCode - Filtered on Zipcode 98014 (as example of zipcode filter) ](https://api.seattlematrix.org/v1/kingcounty/covid-19/zipcode/98104)
    - sample source url https://www.kingcounty.gov/depts/health/covid-19/data/~/media/depts/health/communicable-diseases/documents/C19/data/covid-data-extract-geography-august-24.ashx
