""" save this file as creds.py and update with your
db information. I will be updating app to use Vault 
for secrets soon.
"""

dblogin = {
        'host' : 'localhost',
        'port' : '5432',
        'username' : 'username',
        'password' : 'password',
        'db' : 'db',
        'sslmode' : 'prefer'
}